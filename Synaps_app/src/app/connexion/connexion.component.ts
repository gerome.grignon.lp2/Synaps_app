import { Component } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilisateurService} from "../SERVICES/utilisateur/utilisateur.service";



@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent {

  email = new FormControl('');
  utilisateur : any;
  constructor(private UtilisateurService: UtilisateurService) {
  }

  ngOnInit(){
    this.utilisateur = this.UtilisateurService.getPrenomUtilisateur(1);
    console.log(this.utilisateur);

  }


}
