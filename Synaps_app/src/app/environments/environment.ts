export const environment = {
  production: false, // Indique que c'est l'environnement de développement
  apiUrl: 'http://localhost:8080', // URL de l'API pour le développement
};
