import dotenv from 'dotenv'
dotenv.config()

import express from 'express'
import userRouter from "./routes/userRouter";
import lieuRouter from "./routes/lieuRouter";
import eventRouter from "./routes/eventRouter";
import tarifRouter from "./routes/tarifRouter";

const cors = require('cors');
const app = express();

// Configurer les en-têtes CORS
app.use(cors());

// Autres routes et configurations de votre serveur...
const port = process.env.PORT || 8080;


app.use(express.json())
app.use('/user', userRouter)
app.use('/lieu', lieuRouter)
app.use('/event', eventRouter)
app.use('/tarif', tarifRouter)

app.listen(process.env.PORT, () =>{
    console.log(`Server is running on port ${process.env.PORT}`)
})