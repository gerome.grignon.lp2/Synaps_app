interface Tarif {
    id : number,
    phase_une : number,
    phase_deux : number,
    phase_trois :number,
    phase_quatre : number,
    event_id : number
}

export default Tarif