interface Lieu {
    id : number,
    ville : string,
    cp : number,
    adresse :string,
    libelle : string
}

export default Lieu