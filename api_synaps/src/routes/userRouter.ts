import express, {Request, Response} from 'express'
import * as userModel from '../models/user'
import User from "../types/user"
import {ifError} from "assert";

const userRouter = express.Router()
userRouter.get('/', async(req: Request, res : Response) => {
    userModel.findAll((error: Error, users: User[]) => {
        if (error) {
           return res.status(500).json({"errorMessage": error.message})
        }
        return res.status(200).json({"data": users})
    })
})

userRouter.post('/', async (req : Request, res : Response) =>{
    const newUserNom : string = req.body.nom
    const newUserPrenom : string = req.body.prenom
    const newUserMail : string = req.body.mail
    const newUserMdp : string = req.body.mdp
    userModel.createUser(newUserNom, newUserPrenom, newUserMail, newUserMdp, (error: Error, userId : number) =>{
        if(error){
           return res.status(500).json({"errorMessage": error.message})
        }
        return res.status(200).json({userId})
    })
})

userRouter.get('/:id', async(req : Request, res : Response)=>{
    const userId : number = Number(req.params.id)
    userModel.findUser(userId, (error:Error, user: User) => {
        if(error){
           return res.status(500).json({"errorMessage": error.message})
        }
        return res.status(200).json({"data": user})
    })
})

userRouter.put('/:id/nom', async(req : Request, res : Response)=>{
    const user : User = req.body
    userModel.updateNomUser(user, (error : Error)=>{
        if(error){
            return res.status(500).json({"errorMessage": error.message})
        }
       return res.status(200).send()

    })
})

userRouter.put('/:id/prenom', async(req : Request, res : Response)=>{
    const user : User = req.body
    userModel.updatePrenomUser(user, (error : Error)=>{
        if(error){
            return res.status(500).json({"errorMessage": error.message})
        }
        return res.status(200).send()

    })
})

userRouter.put('/:id/mail', async(req : Request, res : Response)=>{
    const user : User = req.body
    userModel.updateMailUser(user, (error : Error)=>{
        if(error){
            return res.status(500).json({"errorMessage": error.message})
        }
        return res.status(200).send()

    })
})

userRouter.put('/:id/mdp', async(req : Request, res : Response)=>{
    const user : User = req.body
    userModel.updateMdpUser(user, (error : Error)=>{
        if(error){
            return res.status(500).json({"errorMessage": error.message})
        }
       return res.status(200).send()

    })
})

userRouter.delete('/:id', async(req : Request, res : Response) =>{
    const userId : number = Number(req.params.id)
    userModel.deleteUser(userId, (error : Error) =>{
        if(error){
            return res.status(500).json({"errorMessage": error.message})
        }
        return res.status(200).send()
    })
})
export default userRouter