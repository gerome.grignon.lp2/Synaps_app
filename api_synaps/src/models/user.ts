import User from "../types/user";
import connection from "../db-config";
import {ResultSetHeader, RowDataPacket} from "mysql2";
import {callbackify} from "util";
import {query} from "express";

export const createUser = (nom: string, prenom: string, mail: string, mdp: string,  callback: Function)=>{
    const queryString = 'INSERT INTO user (usr_nom, usr_prenom, usr_mail, usr_mdp) VALUES(?,?,?,?)'
    connection.query(queryString, [nom, prenom, mail, mdp], (error, result)=>{
        if (error){
            callback(error)
        }
        const insertId = (<ResultSetHeader> result).insertId
        callback(null, insertId)
    })
}

export const findUser = (userId: number, callback : Function) => {
    const queryString = 'SELECT * FROM user where usr_id = ?'
    connection.query(queryString, [userId], (error, result) =>{
        if (error){
            callback(error)
        }
        const row = (<RowDataPacket>result)[0]
        callback(null, row);
    })
}

export const findAll = (callback : Function) =>{
    const queryString = 'SELECT * FROM user'
    connection.query(queryString, (error,result) => {
        if (error){
            callback(error)
        }
        const rows =<RowDataPacket[]> result
        const users : User[] = []

        rows.forEach(row =>{
            const user: User = {
                id: row.usr_id,
                nom: row.usr_nom,
                prenom: row.usr_prenom,
                mail: row.usr_mail,
                mdp : row.usr_mdp
            }
            users.push(user)
        })
        callback(null, users)
    })
}



export const updateMdpUser = (user : User,  callback: Function) =>{
    const queryString = 'UPDATE user SET usr_mdp = ? WHERE usr_id = ?'
    connection.query(queryString, [user.mdp, user.id], (error, result) =>{
        if(error){
            callback(error)
        }
        callback(null)
    })
}

export const updateMailUser = (user : User,  callback: Function) =>{
    const queryString = 'UPDATE user SET usr_mail = ? WHERE usr_id = ?'
    connection.query(queryString, [user.mail, user.id], (error, result) =>{
        if(error){
            callback(error)
        }
        callback(null)
    })
}

export const updatePrenomUser = (user : User,  callback: Function) =>{
    const queryString = 'UPDATE user SET usr_prenom = ? WHERE usr_id = ?'
    connection.query(queryString, [user.prenom, user.id], (error, result) =>{
        if(error){
            callback(error)
        }
        callback(null)
    })
}

export const updateNomUser = (user : User,  callback: Function) =>{
    const queryString = 'UPDATE user SET usr_nom = ? WHERE usr_id = ?'
    connection.query(queryString, [user.nom, user.id], (error, result) =>{
        if(error){
            callback(error)
        }
        callback(null)
    })
}

export const deleteUser = (userId: number, callback: Function) => {
    const queryString = 'DELETE FROM user WHERE usr_id = ?'
    connection.query(queryString, [userId], (error, result)=> {
        if(error){
            callback(error)
        }
        callback(null)
    })
}

