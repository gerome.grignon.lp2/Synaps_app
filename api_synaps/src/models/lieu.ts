import Lieu from "../types/lieu";
import connection from "../db-config";
import {ResultSetHeader, RowDataPacket} from "mysql2";

export const createLieu = (ville: string, cp: string, libelle: string, adresse: string,  callback: Function)=>{
    const queryString = 'INSERT INTO lieu (lie_ville, lie_CP, lie_libelle, lie_adresse) VALUES(?)'
    connection.query(queryString, [ville, cp, libelle, adresse], (error, result)=>{
        if (error){
            callback(error)
        }
        const insertId = (<ResultSetHeader> result).insertId
        callback(null, insertId)
    })
}