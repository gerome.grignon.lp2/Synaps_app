"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const userModel = __importStar(require("../models/user"));
const userRouter = express_1.default.Router();
userRouter.get('/', async (req, res) => {
    userModel.findAll((error, users) => {
        if (error) {
            return res.status(500).json({ "errorMessage": error.message });
        }
        return res.status(200).json({ "data": users });
    });
});
userRouter.post('/', async (req, res) => {
    const newUserNom = req.body.nom;
    const newUserPrenom = req.body.prenom;
    const newUserMail = req.body.mail;
    const newUserMdp = req.body.mdp;
    userModel.createUser(newUserNom, newUserPrenom, newUserMail, newUserMdp, (error, userId) => {
        if (error) {
            return res.status(500).json({ "errorMessage": error.message });
        }
        return res.status(200).json({ userId });
    });
});
userRouter.get('/:id', async (req, res) => {
    const userId = Number(req.params.id);
    userModel.findUser(userId, (error, user) => {
        if (error) {
            return res.status(500).json({ "errorMessage": error.message });
        }
        console.log(JSON.stringify(user));
        return res.status(200).json(user);
    });
});
userRouter.put('/:id/nom', async (req, res) => {
    const user = req.body;
    userModel.updateNomUser(user, (error) => {
        if (error) {
            return res.status(500).json({ "errorMessage": error.message });
        }
        return res.status(200).send();
    });
});
userRouter.put('/:id/prenom', async (req, res) => {
    const user = req.body;
    userModel.updatePrenomUser(user, (error) => {
        if (error) {
            return res.status(500).json({ "errorMessage": error.message });
        }
        return res.status(200).send();
    });
});
userRouter.put('/:id/mail', async (req, res) => {
    const user = req.body;
    userModel.updateMailUser(user, (error) => {
        if (error) {
            return res.status(500).json({ "errorMessage": error.message });
        }
        return res.status(200).send();
    });
});
userRouter.put('/:id/mdp', async (req, res) => {
    const user = req.body;
    userModel.updateMdpUser(user, (error) => {
        if (error) {
            return res.status(500).json({ "errorMessage": error.message });
        }
        return res.status(200).send();
    });
});
userRouter.delete('/:id', async (req, res) => {
    const userId = Number(req.params.id);
    userModel.deleteUser(userId, (error) => {
        if (error) {
            return res.status(500).json({ "errorMessage": error.message });
        }
        return res.status(200).send();
    });
});
exports.default = userRouter;
