"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const express_1 = __importDefault(require("express"));
const userRouter_1 = __importDefault(require("./routes/userRouter"));
const lieuRouter_1 = __importDefault(require("./routes/lieuRouter"));
const eventRouter_1 = __importDefault(require("./routes/eventRouter"));
const tarifRouter_1 = __importDefault(require("./routes/tarifRouter"));
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use('/user', userRouter_1.default);
app.use('/lieu', lieuRouter_1.default);
app.use('/event', eventRouter_1.default);
app.use('/tarif', tarifRouter_1.default);
app.listen(process.env.PORT, () => {
    console.log(`Server is running on port ${process.env.PORT}`);
});
