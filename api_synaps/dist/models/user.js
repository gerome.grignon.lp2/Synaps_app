"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.updateNomUser = exports.updatePrenomUser = exports.updateMailUser = exports.updateMdpUser = exports.findAll = exports.findUser = exports.createUser = void 0;
const db_config_1 = __importDefault(require("../db-config"));
const createUser = (nom, prenom, mail, mdp, callback) => {
    const queryString = 'INSERT INTO user (usr_nom, usr_prenom, usr_mail, usr_mdp) VALUES(?,?,?,?)';
    db_config_1.default.query(queryString, [nom, prenom, mail, mdp], (error, result) => {
        if (error) {
            callback(error);
        }
        const insertId = result.insertId;
        callback(null, insertId);
    });
};
exports.createUser = createUser;
const findUser = (userId, callback) => {
    const queryString = 'SELECT * FROM user where usr_id = ?';
    db_config_1.default.query(queryString, [userId], (error, result) => {
        if (error) {
            callback(error);
        }
        const row = result[0];
        const user = {
            id: row.usr_id,
            nom: row.usr_nom,
            prenom: row.usr_prenom,
            mail: row.usr_mail,
            mdp: row.usr_mdp
        };
        callback(null, user);
    });
};
exports.findUser = findUser;
const findAll = (callback) => {
    const queryString = 'SELECT * FROM user';
    db_config_1.default.query(queryString, (error, result) => {
        if (error) {
            callback(error);
        }
        const rows = result;
        const users = [];
        rows.forEach(row => {
            const user = {
                id: row.usr_id,
                nom: row.usr_nom,
                prenom: row.usr_prenom,
                mail: row.usr_mail,
                mdp: row.usr_mdp
            };
            users.push(user);
        });
        callback(null, users);
    });
};
exports.findAll = findAll;
const updateMdpUser = (user, callback) => {
    const queryString = 'UPDATE user SET usr_mdp = ? WHERE usr_id = ?';
    db_config_1.default.query(queryString, [user.mdp, user.id], (error, result) => {
        if (error) {
            callback(error);
        }
        callback(null);
    });
};
exports.updateMdpUser = updateMdpUser;
const updateMailUser = (user, callback) => {
    const queryString = 'UPDATE user SET usr_mail = ? WHERE usr_id = ?';
    db_config_1.default.query(queryString, [user.mail, user.id], (error, result) => {
        if (error) {
            callback(error);
        }
        callback(null);
    });
};
exports.updateMailUser = updateMailUser;
const updatePrenomUser = (user, callback) => {
    const queryString = 'UPDATE user SET usr_prenom = ? WHERE usr_id = ?';
    db_config_1.default.query(queryString, [user.prenom, user.id], (error, result) => {
        if (error) {
            callback(error);
        }
        callback(null);
    });
};
exports.updatePrenomUser = updatePrenomUser;
const updateNomUser = (user, callback) => {
    const queryString = 'UPDATE user SET usr_nom = ? WHERE usr_id = ?';
    db_config_1.default.query(queryString, [user.nom, user.id], (error, result) => {
        if (error) {
            callback(error);
        }
        callback(null);
    });
};
exports.updateNomUser = updateNomUser;
const deleteUser = (userId, callback) => {
    const queryString = 'DELETE FROM user WHERE usr_id = ?';
    db_config_1.default.query(queryString, [userId], (error, result) => {
        if (error) {
            callback(error);
        }
        callback(null);
    });
};
exports.deleteUser = deleteUser;
