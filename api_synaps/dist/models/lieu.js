"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createLieu = void 0;
const db_config_1 = __importDefault(require("../db-config"));
const createLieu = (ville, cp, libelle, adresse, callback) => {
    const queryString = 'INSERT INTO lieu (lie_ville, lie_CP, lie_libelle, lie_adresse) VALUES(?)';
    db_config_1.default.query(queryString, [ville, cp, libelle, adresse], (error, result) => {
        if (error) {
            callback(error);
        }
        const insertId = result.insertId;
        callback(null, insertId);
    });
};
exports.createLieu = createLieu;
